package com.muhammadiqbal_10191056.praktikum1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.muhammadiqbal_10191056.praktikum1.ContentActivity;
import com.muhammadiqbal_10191056.praktikum1.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btnIntent = findViewById(R.id.btnlogin);

        btnIntent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent contentIntent = new Intent(MainActivity.this, ContentActivity.class);

                startActivity(contentIntent);
            }
        });
    }
}